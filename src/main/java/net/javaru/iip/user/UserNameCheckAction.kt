package net.javaru.iip.user

import com.intellij.icons.AllIcons
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.CustomShortcutSet
import com.intellij.openapi.diagnostic.logger
import com.intellij.openapi.ide.CopyPasteManager
import com.intellij.openapi.project.DumbAwareAction
import com.intellij.openapi.project.Project
import com.intellij.openapi.ui.DialogBuilder
import com.intellij.ui.components.JBLabel
import org.intellij.lang.annotations.Language
import java.awt.Cursor
import java.awt.datatransfer.StringSelection
import java.awt.event.MouseAdapter
import java.awt.event.MouseEvent
import javax.swing.JPanel

class UserNameCheckAction : DumbAwareAction()
{
    private val LOG = logger<UserNameCheckAction>()

    override fun actionPerformed(e: AnActionEvent)
    {
        val userName = System.getProperty("user.name", "<undetermined>")
        val project = e.project
        val builder = createDialogBuilder(userName, project)
        builder.show()
    }

    private fun createDialogBuilder(userName: String, project: Project?): DialogBuilder
    {
        val panel = createPanel(userName)
        val builder = project?.let { DialogBuilder(it) } ?: DialogBuilder()
        builder.title("User Name")
        builder.centerPanel(panel)
        create { x: AnActionEvent? -> copyInfoToClipboard(userName) }
            .registerCustomShortcutSet(CustomShortcutSet.fromString("meta C", "control C"), builder.dialogWrapper.rootPane)
        return builder
    }

    private fun createPanel(userName: String): JPanel
    {
        val panel = JPanel()
        panel.add(createTextLabel(userName))
        panel.add(createIconLabel(userName))
        return panel
    }

    private fun createTextLabel(userName: String): JBLabel
    {
        val fonts = """'JetBrains Mono','DejaVu Sans Mono','Consolas','Source Code Pro',monospace"""
        @Suppress("HtmlRequiredLangAttribute")
        @Language("HTML")
        val text =
            ("""<html><span style='font-size: larger'>User name: <strong><span style="font-family: $fonts">$userName</span></strong></html>""")
        return JBLabel(text)
    }

    private fun createIconLabel(userName: String): JBLabel
    {
        val iconLabel = JBLabel(AllIcons.Actions.Copy)
        iconLabel.addMouseListener(object : MouseAdapter()
                                   {
                                       override fun mouseClicked(mouseEvent: MouseEvent)
                                       {
                                           if (iconLabel.contains(mouseEvent.point))
                                           {
                                               copyInfoToClipboard(userName)
                                           }
                                       }

                                       override fun mouseEntered(mouseEvent: MouseEvent)
                                       {
                                           iconLabel.cursor = Cursor.getPredefinedCursor(Cursor.HAND_CURSOR)
                                       }

                                       override fun mouseExited(e: MouseEvent)
                                       {
                                           iconLabel.cursor = Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR)
                                       }
                                   })
        iconLabel.toolTipText = "Copy user name to clipboard"
        return iconLabel
    }


    private fun copyInfoToClipboard(text: String)
    {

        try
        {
            CopyPasteManager.getInstance().setContents(StringSelection(text))
        }
        catch (e: Exception)
        {
            LOG.info("Could not copy user name to clipboard due to an exception: $e")
        }
    }

}