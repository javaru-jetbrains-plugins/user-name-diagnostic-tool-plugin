package net.javaru.iip.user

import com.intellij.ide.AppLifecycleListener
import com.intellij.openapi.diagnostic.logger


class UserNameCheck : AppLifecycleListener
{
    private val LOG = logger<UserNameCheck>()
    /*
        1. appFrameCreated()
        2. welcomeScreenDisplayed()
            • Obviously only called is the welcome screen is displayed rather than a project immediately
            • It is NOT called again if all projects are closed and you return to the Welcome screen, even id Welcome screen was not initially displayed on startup
        3. appStarting()
        4. appStarted() !!!!INTERNAL USE ONLY!!!!

        5. appClosing()
        6. appWillBeClosed()
    */



    override fun appFrameCreated(commandLineArgs: MutableList<String>)
    {
        super.appFrameCreated(commandLineArgs)
        var userName = System.getProperty("user.name", "<undetermined>") ?: "<null-value>"
        if (userName.isEmpty()) userName = "<empty-string>"
        if (userName.isBlank()) userName = "<blank-string>"
        logger<UserNameCheck>().info("User Name configured as: $userName")
    }
}