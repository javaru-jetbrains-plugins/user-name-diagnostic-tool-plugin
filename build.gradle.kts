import org.jetbrains.intellij.tasks.PatchPluginXmlTask
import org.jetbrains.intellij.tasks.RunIdeTask
import org.jetbrains.intellij.tasks.RunPluginVerifierTask

val targetJavaVersion: JavaVersion = JavaVersion.VERSION_11

plugins {
    // https://github.com/JetBrains/gradle-intellij-plugin
    id("org.jetbrains.intellij") version "0.7.3"
    java
    kotlin("jvm") version "1.4.32"
}

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib"))
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.7.1")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")
}



/** allows for the standard task syntax after declaring something like:  val runIde: RunIdeTask by tasks. */
inline operator fun <T : Task> T.invoke(a: T.() -> Unit): T = apply(a)
val patchPluginXml: PatchPluginXmlTask by tasks
val runIde: RunIdeTask by tasks
val runPluginVerifier: RunPluginVerifierTask by tasks
intellij {
    // See https://github.com/JetBrains/gradle-intellij-plugin/
    version = "2021.1"

    patchPluginXml {
        version("1.0.0")
        sinceBuild("201.6668") // v2020.1 is first version that has the HelpDiagnosticTools action group
        untilBuild("999.*")
        changeNotes(
            """
      v1.0.0: Initial release
 """
                   )
    }

    runIde {
        // Use the following line to test the setting of alternative user names
        this.systemProperties = mapOf("user.name" to "GraceHopper")
    }

    runPluginVerifier {
        // Reports appear in ${project.buildDir}/reports/pluginVerifier by default. Set `verificationReportsDirectory` to change
        ideVersions(listOf("IC-2020.1.4", "IC-2020.2.4", "IC-2020.3.4", "IC-2021.1"))
    }
}

tasks.getByName<Test>("test") {
    useJUnitPlatform()
}

java {

    sourceCompatibility = targetJavaVersion
    targetCompatibility = targetJavaVersion
}

tasks {
    withType<JavaCompile> {
        options.encoding = Charsets.UTF_8.name()
    }

    withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
        all {
            kotlinOptions {
                jvmTarget = targetJavaVersion.toString()
                javaParameters = true
                //noReflect = false
            }
        }
    }
}


